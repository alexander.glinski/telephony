package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.websystique.springmvc.dao.PhoneDao;
import com.websystique.springmvc.model.Phone;

@Service("phoneService")
@Transactional
public class PhoneServiceImpl implements PhoneService {
	
	@Autowired
	private PhoneDao phoneDao;

	@Override
	public List<Phone> findByUserId(int userId) {
		return phoneDao.findByUserId(userId);
	}

	@Override
	public Phone findById(int id) {
		return phoneDao.findById(id);
	}

	@Override
	public void save(Phone phone) {
		phoneDao.save(phone);
	}

}
