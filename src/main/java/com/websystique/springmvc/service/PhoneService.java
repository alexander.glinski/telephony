package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.Phone;

public interface PhoneService {
	
	List<Phone> findByUserId(int userId);
	Phone findById(int id);
	void save(Phone phone);	

}
