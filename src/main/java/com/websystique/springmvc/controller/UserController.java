package com.websystique.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.websystique.springmvc.model.Phone;
import com.websystique.springmvc.model.Request;
import com.websystique.springmvc.model.Response;
import com.websystique.springmvc.service.PhoneService;

@RestController
@RequestMapping("/test")
public class UserController {
	
	@Autowired
	PhoneService phoneService;
	
	@RequestMapping(value = "/hello",method = RequestMethod.GET)
	public Response getInfo(@RequestParam(value="key") String key) {
		if(key.equals("TEST")) {
			Request  req = new Request();
			req.setId(100);
			req.setData("TEXT");
			
			Response  response = new Response();
			response.setCode(50);
			response.setDetail("OK");
			return response;
		}
		Response  response = new Response();
		response.setCode(-1);
		response.setDetail("NOT OK");
		return response;
	}
	
	@RequestMapping(value = "/listPhones",method = RequestMethod.GET)
	public List<Phone> getPhonesByUserId(@RequestParam(value="ownerId") String ownerId) {
		List<Phone> list = phoneService.findByUserId(Integer.parseInt(ownerId));		
		return list;
	}

}
