package com.websystique.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.Phone;

@Repository("phoneDao")
public class PhoneDaoImp extends AbstractDao<Integer, Phone> implements PhoneDao{
	
	static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Phone> findByUserId(int userId) {
		logger.info("findByUserId(int userId)");
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("ownerId", userId));
		List<Phone> list = criteria.list();
		return list;
	}

	@Override
	public Phone findById(int id) {
		return getByKey(id);
	}

	@Override
	public void save(Phone phone) {
		persist(phone);
	}

}
