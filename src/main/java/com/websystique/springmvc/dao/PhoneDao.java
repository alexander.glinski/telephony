package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.Phone;

public interface PhoneDao {
	
	List<Phone> findByUserId(int userId);
	Phone findById(int id);
	void save(Phone phone);		

}
