<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Users List</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<div class="generic-container">
		<%@include file="authheader.jsp" %>	
		<div class="panel panel-default">
			  <!-- Default panel contents -->
		  	<div class="panel-heading"><span class="lead">List of Phones </span></div>
			<table class="table table-hover">
	    		<thead>
		      		<tr>
				        <th>ID</th>
				        <th>TYPE</th>
				        <th>AREA CODE</th>
				        <th>PHONE NUMBER</th>
				        <th width="100"></th>
				        <th width="100"></th>
					</tr>
		    	</thead>
	    		<tbody>
				<c:forEach items="${phones}" var="phone">
					<tr>
						<td>${phone.id}</td>
						<td>${phone.type}</td>
						<td>${phone.areaCode}</td>
						<td>${phone.pNumber}</td>
							<td><a href="<c:url value='/edit-phone-${phone.id}' />" class="btn btn-success custom-width">edit</a></td>
							<td><a href="<c:url value='/delete-phone-${phone.id}' />" class="btn btn-danger custom-width">delete</a></td>
					</tr>
				</c:forEach>
	    		</tbody>
	    	</table>
		</div>
		 	<div class="well">
		 		<a href="<c:url value='/newphone' />">Add New Phone</a>
		 	</div>
   	</div>
</body>
</html>